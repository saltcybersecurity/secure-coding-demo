# Secure Coding Demo
## Inhoudsopgave
- [Juice Shop](#markdown-header-juice-shop)
- [Voorbereiding](#markdown-header-voorbereiding)
- [Installatie](#markdown-header-installatie)
- [Tools](#markdown-header-tools)
- [Uitdagingen](#markdown-header-uitdagingen)
  - [1. DOM XSS](#markdown-header-1-dom-xss)
  - [2. Zero Stars](#markdown-header-2-zero-stars)
  - [3. Login Admin](#markdown-header-3-login-admin)
  - [4. View Basket](#markdown-header-4-view-basket)
  - [5. Meta Geo Stalking](#markdown-header-5-meta-geo-stalking)
  - [6. Admin Registration](#markdown-header-6-admin-registration)
  - [7. Database Schema](#markdown-header-7-database-schema)
  - [8. Upload Size](#markdown-header-8-upload-size)
  - [9. Upload Type](#markdown-header-9-upload-type)
  - [10. Product Tampering](#markdown-header-10-product-tampering)
  - [11. Manipulate Basket](#markdown-header-11-manipulate-basket)
  - [12. Client-side XSS Protection](#markdown-header-12-client-side-xss-protection)
  - [13. Expired Coupon](#markdown-header-13-expired-coupon)
  - [14. Kill Chatbot](#markdown-header-14-kill-chatbot)
  - [15. Forged Signed JWT](#markdown-header-15-forged-signed-jwt)
- [Hints](#markdown-header-hints)
- [Oplossingen](#markdown-header-oplossingen)

## Juice Shop
> "OWASP Juice Shop is probably the most modern and sophisticated insecure web application! It can be used in security trainings, awareness demos, CTFs and as a guinea pig for security tools! Juice Shop encompasses vulnerabilities from the entire OWASP Top Ten along with many other security flaws found in real-world applications!"

De Juice Shop is geschreven in Node.js, Express and het Angular Framework. Kennis van deze frameworks is niet vereist bij het oplossen van de uitdagingen, maar kan wel helpen. 

De applicatie bevat een 100 tal aan kwetsbaarheden die d.m.v. challenges opgelost kunnen worden. Voor deze workshop zijn er een 15 uitdagingen geselecteerd die door jullie gevonden en eventueel opgelost moeten worden. (Zie verder onder [Uitdagingen](#markdown-header-uitdagingen))

Dit is een aangepast versie van de Juice Shop voor workshop doeleinden. Wil je meer te weten komen over de Juice Shop of je verder verdiepen in de andere uitdagingen dan kan je hier terecht: [Juice Shop](https://owasp.org/www-project-juice-shop/)

## Voorbereiding
Voor deze hands-on lab oefening is een docker omgeving met docker-compose nodig.  
Kies één van de volgende opties om dit voor te bereiden. Of sla deze stap over als je al over docker en docker-compose beschikt.  

**Docker binnen een nieuwe Virtual Machine met Ubuntu Server** 

* Volg de stappen zoals beschreven in: [ubuntu-server-instructies.md](/ubuntu-server-instructies.md)    

**Docker binnen een eigen omgeving** 

* Installeer Docker: [Docker download](https://docs.docker.com/get-docker/)
* Installeer Docker Compose: [Docker compose install](https://docs.docker.com/compose/install/)

**Applicatie runnen zonder docker / docker-copose**

* Installeer Nodejs versie 14-18


## Installatie

1. Voer het volgende commando uit: `git clone https://bitbucket.org/saltcybersecurity/secure-coding-demo.git`.
2. Open de folder via het commando `cd secure-coding-demo`.

***DOCKER***
3. Start Docker.
4. Installeer en run de applicatie. Dit kan op de voorgrond `docker-compose up --build` of achtergrond `docker-compose up --build -d`.
5. Aanpassen hostfile: `[localhost / VP adres] juiceshop`.
5. Openen de Juice Shop via [juiceshop:3000](http://juiceshop:3000).

***NPM***
3. Run `npm i`
4. Run `npm run start`
5. Openen de Juice Shop via [juiceshop:3000](http://juiceshop:3000).

> Tijdens de opdrachten is het nodig om docker opnieuw te laten builden. Hiervoor kan je beter eerst de docker service down brengen. Dit kan met het volgende commando: `docker-compose down`.

> Bij het compileren zullen er een aantal errors naar boven komen. Deze kun je negeren bij het starten van de applicatie.

Voorbeeld van de juice shop homepagina na installatie:

![Juice Shop](/frontend/src/assets/public/juice-shop.png "Juice Shop")

## Tools
1. Code editor ([VS Code](https://code.visualstudio.com/))
2. REST client ([cUrl](https://curl.se/) / [Postman](https://www.postman.com/))
3. Latest browser ([EDGE](https://www.microsoft.com/en-us/edge?r=1) / [Chrome](https://www.google.com/chrome/) / [Firefox](https://www.mozilla.org/en-US/firefox/new/))

## Uitdagingen
Voor de sercure coding module hebben we een aantal uitdagingen uitgekozen die jullie voor deze workshop moeten opsporen. Het is daarnaast de bedoeling dat voor elke gevonden "vulnerability" ook een oplossing bedacht wordt.

De uitdagingen zijn onderverdeeld in categorieen aan de hand van moeilijkheid. Hierbij is een 1 ster een makkelijke uitdaging en een 6 ster de moeilijkste.

Onderaan de uitdaging kan je hints vinden. Mocht je vastlopen bij een uitdaging dan kan je hier wat meer informatie vinden om verder te komen.

Bij het behalen van een uitdaging zal er een popup verschijnen met daarin de behaalde challenge.
Om te zien of je een uitdaging al gehaald hebt kun je de score board pagina bekijken <http://juiceshop:3000/#/score-board>.

![Passed challenge](/frontend/src/assets/public/passed.png "Passed challenge")

---
![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")

### 1. DOM XSS
Niet alle invoer velden op de website zijn beschermd tegen XSS. Probeer een iframe met een alert text ```<iframe src="javascript:alert(`xss`)">``` op een pagina te krijgen d.m.v. Cross Site Scripting.

Meer informatie over [DOM XSS](https://owasp.org/www-community/attacks/DOM_Based_XSS)


### 2. Zero Stars
Het feedback formulier geeft de gebruiker de mogelijkheid om een review achter te laten met een ster rating. In de huidige situatie is het alleen mogelijk een 1 to 5 ster rating te geven. Voor deze uitdaging is het de bedoeling een 0 ster rating achter te laten op de website.


---
![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")
### 3. Login Admin
Naast de standaard klant accounts op de juice shop is er ook een administrator account. 
Probeer hiervan het emailadress te achterhalen en daarmee in te loggen. De challenge is op meerdere manieren op te lossen.


### 4. View Basket
Bij het bestellen van producten in de juice shop blijken de winkelmandjes niet helemaal prive te zijn. 
Bekijk de inhoud van iemand anders zijn winkelmandje.

### 5. Meta Geo Stalking
Probeer het antwoord op John zijn geheime vraag te achterhalen en gebruik dit om zijn wachtwoord te resetten. 
Open de wachtwoord vergeten pagina om zijn geheime vraag te tonen.

---
![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")
### 6. Admin Registration
Het registratie formulier geeft de gebruiker de mogelijkheid om een gebruikers account aan te maken voor de juice shop. Probeer dit formulier te gebruiken om in plaats van een gebruiker een administrator account te registreren.


### 7. Database Schema
Exfiltreer de gehele database schema definitie via SQL injectie.


### 8. Upload Size
Het feebackformulier geeft de gebruiker de mogelijkheid om een file te uploaden. Deze file mag een maximale limiet van 100KB hebben. 
Voor deze uitdaging is het de bedoeling een file te uploaden met een grote tussen de 101 en 199KB. (Meer dan 200KB zal een 500 error geven.)

### 9. Upload Type
In uitdaging 7. Upload Size hebben we al de grote van een file proberen te omzeilen. In deze uitdading is het de bedoeling een file te uploaden met een andere extensie dan PDF/Zip.


### 10. Product Tampering
Verander het ```href``` attribute van het product "OWASP SSL Advanced Forensic Tool (O-Saft)" naar: https://owasp.slack.com


### 11. Manipulate Basket
In de uitdaging "4. View Basket" hebben we al een kijkje kunnen nemen in iemand anders zijn winkelmandje. 
Nu is de volgende uitdaging een extra product toe te voegen aan het iemand anders zijn winkelmandje.



### 12. Client-side XSS Protection

> Deze opdracht werkt niet bij gebruik van Docker.

Voer een persisted XSS aanval uit op de website d.m.v. van ```<iframe src="javascript:alert(`xss`)">``` en hiermee de client-side beveiliging te omzeilen. Persisted wil zeggen dat een stukje javascript op de server wordt opgeslagen.



---
![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")
### 13. Expired Coupon
Het is in de juice shop mogelijk om coupons te gebruiken bij het afrekenen. 
Deze coupons worden met verschillende events/campagnes weggegeven.
Probeer 1 van deze coupon codes te achterhalen en gebruik deze bij het afrekenen.


---
![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")
### 14. Kill Chatbot
Probeer de chatbot uit te schakelen.


---
![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")![1 Ster](/frontend/src/assets/public/star-icon.jpeg "1 Ster")
### 15. Forged Signed JWT
Probeer een bijna juist RSA-signed JWT Token te genereren dat de gebruiker rsa_lord@juice-sh.op nabootst.



## Hints
### 1. DOM XSS - Hints
> Hint: Sommige input velden tonen data direct op het scherm na het versturen. 

### 2. Zero Stars - Hints
> Hint: Inspecteer het formulier d.m.v. de browser inspector. Misschien dat hier waardes aan te passen te zijn of te verwijderen.

> Hint: Bekijk het request en probeer dit zelf via Postman te versturen.

### 3. Login Admin - Hints
> Hint: Misschien dat de admin een product review heeft geplaatst.

> Hint: Je kan dit proberen te gokken. Denk hierbij niet te moeilijk na. (Brute force)
>
> Tip over sterke [wachtwoorden](https://blog.avast.com/strong-password-ideas)

> Hint: Maak gebruik van een SQL injectie. Google voor bekende SQL injecties op gebruikersnaam + wachtwoord.
>
> Meer informatie over [sql-inject](https://owasp.org/www-community/attacks/SQL_Injection)

### 4. View Basket - Hints
> Hint: Neem is een kijk in de cookies/session data.

> Hint: Wat gebeurd er als je de waarde van ```bid``` aanpast.

### 5. Meta Geo Stalking
> Hint: Bekijk de post van John op de Photo Wall.

> Hint: Probeer het plaatje te inspecteren. Wat voor meta data kan je hieruit halen?

### 6. Admin Registration - Hints
> Hint: Bekijk het response voor het registreren van een gebruiker. Welke keys lijken te bepalen dat dit een normale gebruiker is?

### 7. Database Schema - Hints
> Hint: Een GET request naar ```http://juiceshop:3000/rest/products/search?q=apple``` blijkt niet geheel beveiligd te zijn.

> Hint: De juice shop maakt gebruik van een SQLite database. Gebruik deze [link](https://www.sqlite.org/faq.html) voor extra hulp bij de uitdaging

### 8. Upload Size - Hints
> Hint: Probeer te achterhalen hoe het upload formulier werkt. Dit kan door een kleine file te uploaden.

### 9. Upload Type - Hints
> Hint: Probeer te achterhalen hoe het upload formulier werkt. Dit kan door een PDF/Zip file te uploaden.

### 10. Product Tampering - Hints
> Hint: Dit lukt niet met alleen de UI, misschien dat we meer informatie kunnen vinden in de REST call bij het openen van het product.

> Hint: Voor het wijzigen van data wordt de PUT methode gebruikt.

### 11. Manipulate Basket - Hints
> Hint: Dit lukt niet met alleen de UI, misschien dat we meer informatie kunnen vinden in de REST call bij het toevoegen van een item aan het mandje.

> Hint: Wat gebeurd als we twee winkelmandjes tegelijkertijd willen updaten?

### 12. Client-side XSS Protection - Hints
> Hint: Wat als we een van de gebruiker velden aanpassen naar deze waarde.

### 13. Expired Coupon - Hints
> Hint: Misschien dat je javascript files je wat meer kunnen vertellen.

> Hint: Wat als we onze machine tijd wijzigen?

### 14. Kill Chatbot - Hints
> Hint: Misschien dat je meer informatie kunt vinden op de github pagina van de [chatbot](https://github.com/juice-shop/juicy-chat-bot)

### 15. Forged Signed JWT - Hints
> Hint: Gebruik hiervoor Burp Suite

> Hint: [JWT.io](https://jwt.io/)




## Oplossingen
### 1. DOM XSS - Oplossing
> Oplossing: Het zoekveld is hiervoor een oplossing. Op dit veld wordt geen input sanitization gedaan. Bij het versturen zal op de zoekpagina een ```alert``` box worden getoond.

### 2. Zero Stars - Oplossing
> Oplossing: Inspecteer het formulier en verwijder het ```disabled="true"``` attribute van de submit button om de validatie te omzijlen. Dit attribute wordt meegegeven als iemand geen rating invuld op het formulier.

### 3. Login Admin - Oplossing
> Oplossing: De username van de administrator is: admin@juice-sh.op. Door te gokken kan je erachter komen dat het wachtwoord ```admin123``` is. Of je kunt hiervoor SQL injectie gebruiken door ```admin@juice-sh.op';--``` te gebruiken als username en een random wachtwoord. de ```';--``` sluit een query af waardoor er niet langer gekeken wordt naar het wachtwoord.

### 4. View Basket - Oplossing
> Oplossing: Pas de ```bid``` value aan in je session storage en refresh de pagina.

### 5. Meta Geo Stalking - Oplossing
> Oplossing: Donwload het plaatje om deze verder te inspecteren. In de Meta data kan je een geo locatie achterhalen: “36°57’31.38″ N 84°20’53.58″ W”. Bij het invoeren van deze data in google maps kom je al gauw uit op ```Daniel Boone National Forest```.

### 6. Admin Registration - Oplossing
> Oplossing: Verstuur een bij het registreren van een gebruiker een POST request naar ```http://juiceshop:3000/api/Users``` met als aangepaste key ```"role":"admin"```

### 7. Database Schema - Oplossing
> Oplossing: Maak een GET request naar ```http://juiceshop:3000/rest/products/search?q=qwert')) UNION SELECT sql, '2', '3', '4', '5', '6', '7', '8', '9' FROM sqlite_master--```

### 8. Upload Size - Oplossing
> Oplossing: Verstuur een POST request naar ```http://juiceshop:3000/file-upload``` met als form parameter ```file``` en als waarde een PDF/Zip met een grote tussen de 101 en 199KB.


### 9. Upload Type - Oplossing
> Oplossing: Verstuur een POST request naar ```http://juiceshop:3000/file-upload``` met als form parameter ```file``` en als waarde een niet PDF/Zip met een grote tussen de 101 en 199KB.

### 10. Product Tampering - Oplossing
> Oplossing: Voor een PUT request uit naar ```http://juiceshop:3000/api/Products/9``` met als body ```{"description": "<a href=\"https://owasp.slack.com\" target=\"_blank\">More...</a>"}```

### 11. Manipulate Basket - Oplossing
> Oplossing: Voor een POST request uit naar ```http://juiceshop:3000/api/BasketItems``` met als body ```{"ProductId": 14,"BasketId": "2","quantity": 1, "BasketId": "1"}```. Hierbij zijn de BasketIds die van jou en van de gebruiker waar van je het mandje wilt aanpassen.

### 12. Client-side XSS Protection - Oplossing
> Oplossing: Verstuur een POST request naar ```http://juiceshop:3000/api/Users``` met ```{"email": "<iframe src=\"javascript:alert(xss)\">", "password": "xss"}``` als body. Log in als admin om het script te bekijken op de [http://juiceshop:3000/#/administration] pagina.

### 13. Expired Coupon - Oplossing
> Oplossing: In ```main-es2015.js``` kan je meer informatie vinden over de verschillende coupons als je zoekt op ```this.campaigns```. ALs je een coupon kopieerd blijkt deze niet te werken, omdat de validOn niet geldig is. De ```validOn``` value kan vertaald worden naar een javascript date. Bij het aanpassen van je machine tijd naar dezelfde waarde zal de coupon geldig worden.

### 14. Kill Chatbot - Oplossing
> Oplossing: Wijzig je username in ```admin"); process=null; users.addUser("1337", "test```. Hierdoor zal de chatbot automatisch uitgeschakeld worden wanneer je met hem praat.

### 15. Forged Signed JWT - Oplossing
> Oplossing: In het directory ```http://juiceshop:3000/encryptionkeys``` kun je de public JWT key vinden. Download de ```JSON Web Token Attacker``` extension voor Burp Suite. Achterhaal een request met een Authenticatie Bearer token. Klik in de Repeater op de JWS tab en pas in de payload het email adres aan naar ```rsa_lord@juice-sh.op```. Ga naar de ```Attacker tab``` en selecteer in de dropdown ```Key Confusion```. Laad de data van de JWT file in her veld, verwijder hierbij de ```-----BEGIN RSA PUBLIC KEY-----``` en ```-----END RSA PUBLIC KEY-----``` lijnen. Klik op update en verstuur het request.